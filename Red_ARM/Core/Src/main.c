/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * Arquivo           : main.c
  *
  * Descrição         : Código do microcontrolador ARM para os robôs VSSS da
  * 					Equipe Red Dragons UFSCar. Desenvolvido por Matheus
  * 					Sousa Soares em 2020.
  *
  * 					Para contato: matheussousasf@gmail.com ou
  * 								  matheussousa@df.ufscar.br
  * 								  (31) 975340460
  *
  ******************************************************************************
  * Código desenvolvido na CUBE IDE 1.3.0
  *
  * Lista de funções desempenhadas:
  * Comunicação UART por interrupção com módulo Xbee OK
  * Leitura e interpretação da mensagem recebida OK
  * Geração de PWM para motores OK
  * Leitura dos Encoders (Montar circuito)
  * Controle da velocidade dos motores com PWM gerado
  *
  * ****************************************************************************
  * Como utilizar:
  *
  * Editar nas partes com "USER CODE" o resto do código é gerado automaticamente
  * pela parte de configuração do microcontrolador na IDE (arquivo .ioc).
  *
  * Organize e comente as suas funções e códigos adequadamente para facilitar
  * para os membros futuros da equipe. Utilize o Git para atualizar o código.
  *
  * ***************************************************************************
  * Partes do código
  * USER CODE Header: Comentários de introdução
  * USER CODE Includes: Bibliotecas do usuário
  * USER CODE PV: Declaração de variáveis
  * USER CODE 0: Vazio
  * USER CODE 1: Vazio
  * USER CODE SysInit: Inicialização do sistema
  * USER CODE 2: Vazio
  * USER CODE WHILE: Loop de programa
  * USER CODE 3: Vazio
  * USER CODE 4: Funções de interrupção e timers
  *
  * Preencher aqui conforme o código for atualizado, por favor.
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
uint16_t speed = 0;
uint16_t pulse_cnt = 1280; // Contagem de pulsos do clock. É a resolução do PWM
uint16_t duty_cycle = 640;
uint16_t delay = 500;
uint8_t size_msg = 4;
uint8_t tx_buffer []={'L','i','g','a','d','o','\n'}; // Msg inicial enviada pelo robô
uint8_t rx_buffer [4]; //Buffer que recebe as mensagens

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  HAL_GPIO_WritePin(Led_i_GPIO_Port, Led_i_Pin, SET);// Desliga o led da placa de desenvolvimento
  HAL_UART_Receive_IT(&huart1, rx_buffer, size_msg);// Faz com que o robô ligue o receptor de msg
  HAL_UART_Transmit_IT(&huart1, tx_buffer, 7); // envia a msg inicial
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2); // Inicia o Timer 2 do PWM
  HAL_GPIO_WritePin(M2_GPIO_Port, M2_Pin, SET);
  HAL_GPIO_WritePin(M1_GPIO_Port, M1_Pin, RESET);

  /* USER CODE END SysInit */


  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();

  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	 //HAL_GPIO_TogglePin(Led_y_GPIO_Port,Led_y_Pin);

	/*HAL_Delay(delay/2);
	duty_cycle = duty_cycle + pulse_cnt/10;
	if(duty_cycle > pulse_cnt){
		HAL_Delay(delay);
		duty_cycle = 0;
	}*/
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1280;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(Led_i_GPIO_Port, Led_i_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, Led_g_Pin|Led_y_Pin|Led_r_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, M2_Pin|M1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : Led_i_Pin */
  GPIO_InitStruct.Pin = Led_i_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(Led_i_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Led_g_Pin Led_y_Pin Led_r_Pin */
  GPIO_InitStruct.Pin = Led_g_Pin|Led_y_Pin|Led_r_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : M2_Pin M1_Pin */
  GPIO_InitStruct.Pin = M2_Pin|M1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){ // Função chaamada quando o buffer de recepção está CHEIO

	if(rx_buffer[0] == 'D'){// Função vira para a direita

		HAL_GPIO_WritePin(M1_GPIO_Port, M1_Pin, SET);
		HAL_GPIO_WritePin(M2_GPIO_Port, M2_Pin, RESET);
		HAL_GPIO_TogglePin(Led_g_GPIO_Port,Led_g_Pin);
	}
	if(rx_buffer[0] == 'E'){// Função vira para a esquerda

		HAL_GPIO_WritePin(M2_GPIO_Port, M2_Pin, SET);
		HAL_GPIO_WritePin(M1_GPIO_Port, M1_Pin, RESET);
		HAL_GPIO_TogglePin(Led_y_GPIO_Port,Led_y_Pin);

	}
	if(rx_buffer[0] == 'S'){// Função que par aos motores
		HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);
		HAL_GPIO_TogglePin(Led_r_GPIO_Port,Led_r_Pin);
		HAL_GPIO_WritePin(M2_GPIO_Port, M2_Pin, RESET);
		HAL_GPIO_WritePin(M1_GPIO_Port, M1_Pin, RESET);
		speed = 0;
		}
	if(rx_buffer[0] == 'G'){// Função que inicia o PWM
		HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	}
	else{
		__NOP();// Adicionado para fins de Debug portanto não faz nada
	}

	speed = 100*(rx_buffer[1]-'0') + 10*(rx_buffer[2]-'0') + (rx_buffer[3]-'0');// Interpreta a msg e converte em velocidade

	if (speed > 100){
		speed = 100;
	}

	duty_cycle = (speed/100)*pulse_cnt; // Calcula o duty cycle para a velocidade desejada
	htim2.Instance->CCR2 = duty_cycle; // Ajusta o valor do duty cycle do PWM atualizando a velocidade do robô

	/*for(int i = 0; i<4; i++ ){ // Clona e envia a msg recebida
			tx_buffer[i] = rx_buffer[i];
	}
	HAL_UART_Transmit_IT(&huart1, tx_buffer, 1);
	*/

	HAL_UART_Receive_IT(&huart1, rx_buffer, size_msg);// Liga a recepção para a próxima msg

}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){// Função executada depois que a msg é totalmente enviada
	__NOP();

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
